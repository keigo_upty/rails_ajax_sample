Rails.application.routes.draw do
  resources :users do
    get "search", to: "users#search", on: :collection
  end
end
