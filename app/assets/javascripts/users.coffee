$ ->
  $('#search').keyup ->
    $.ajax
      url: 'users/search'
      type: 'GET'
      data: word: $(this).val()
    return
  return
